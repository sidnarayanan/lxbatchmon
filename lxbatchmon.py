#!/usr/bin/env python

from subprocess import Popen,PIPE
from ROOT import TCanvas, TGraph,TLegend,TH1F,THStack
import ROOT
from array import array
import cPickle as pickle
from time import time
from tdrStyle import *
setTDRStyle()

now = time()
qs = ['8nh','1nh','2nw4cores','2nd']
states = ['RUN','PEND']

oldcache = {} # time: {'queue':(RUN,PEND)}
try:
  with open('/afs/cern.ch/user/s/snarayan/www/lxbatchmon/cache.pkl','rb') as infile:
    oldcache = pickle.load(infile)
except:
  pass

NHOURS = 12

cache = {}
for t,v in oldcache.iteritems():
  if now-t<NHOURS*3600:
    cache[t]=v

# add new time point
cache[now] = {}
for q in qs:
  for s in states:
    cache[now][q+'_'+s] = 0
for line in Popen('bjobs',shell=True,stdout=PIPE).stdout.readlines():
  if 'JOBID' in line:
    continue
  split = line.split()
  if len(split)>=4:
    q = split[3]
    s = split[2]
    label = q+'_'+s
    if label in cache[now]:
      cache[now][label] = cache[now][label]+1

with open('/afs/cern.ch/user/s/snarayan/www/lxbatchmon/cache.pkl','wb') as outfile:
  pickle.dump(cache,outfile)

with open('/afs/cern.ch/user/s/snarayan/www/lxbatchmon/log','w') as logfile:
  for t in sorted(cache):
    v = cache[t]
    logfile.write('%f %s\n'%(t,str(v)))

xaxis = []
yaxes = {}
for q in qs:
  for s in states:
    yaxes[q+'_'+s] = []

for t in sorted(cache):
  v = cache[t]
  xaxis.append((t-now)/3600.)
  for l in v:
    yaxes[l].append(v[l])

xarray = array('f',xaxis)
N = len(xaxis)
yarrays = {}
for l in yaxes:
  yarrays[l] = array('f',yaxes[l])

# make plots
c = TCanvas('c','c',1200,600)
leg = TLegend(0.7,0.7,0.9,0.9)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
for q in qs:
  c.Clear()
  leg.Clear()
  hrun = TH1F('hrun'+q,'hrun',N,xarray)
  hpend = TH1F('hpend'+q,'hpend',N,xarray)
  for iT in xrange(N):
    hrun.SetBinContent(hrun.FindBin(xarray[iT]),yarrays[q+'_RUN'][iT])
    hpend.SetBinContent(hpend.FindBin(xarray[iT]),yarrays[q+'_PEND'][iT])
  '''
  grun = TGraph(N,xarray,yarrays[q+'_RUN'])
  gpend = TGraph(N,xarray,yarrays[q+'_PEND'])
  grun.SetLineColor(1); grun.SetLineWidth(3); grun.SetMinimum(0); grun.SetMaximum(max(yaxes[q+'_RUN'])*1.5); grun.GetXaxis().SetTitle('Hours ago'); grun.GetYaxis().SetTitle('Jobs')
  gpend.SetLineColor(2); gpend.SetLineWidth(1); gpend.SetMinimum(0); gpend.SetMaximum(max(yaxes[q+'_PEND'])*1.5); gpend.GetXaxis().SetTitle('Hours ago'); gpend.GetYaxis().SetTitle('Jobs')
  leg.AddEntry(grun,'RUN','l')
  leg.AddEntry(gpend,'PEND','l')
  '''
  hrun.SetLineColor(1);  hrun.SetFillColor(ROOT.kAzure+6); hrun.SetLineWidth(1);  hrun.SetMinimum(0);  hrun.SetMaximum(max(yaxes[q+'_RUN'])*1.5);   hrun.GetXaxis().SetTitle('Hours ago');  hrun.GetYaxis().SetTitle('Jobs')
  hpend.SetLineColor(1); hpend.SetFillColor(ROOT.kTeal+6); hpend.SetLineWidth(1); hpend.SetMinimum(0); hpend.SetMaximum(max(yaxes[q+'_PEND'])*1.5); hpend.GetXaxis().SetTitle('Hours ago'); hpend.GetYaxis().SetTitle('Jobs')
  hs = THStack('hs'+q,'')
  hs.Add(hrun); hs.Add(hpend)
  hs.Draw('hist')
  hs.GetXaxis().SetTitle('Hours ago')
  hs.GetYaxis().SetTitle('NJobs')
  leg.AddEntry(hrun,'RUN','f')
  leg.AddEntry(hpend,'PEND','f')
  '''
  if max(yaxes[q+'_RUN'])>max(yaxes[q+'_PEND']):
    grun.Draw('AL')
    gpend.Draw('same L')
  else:
    gpend.Draw('AL')
    grun.Draw('same L')
  '''
  leg.Draw()
  c.SaveAs('/afs/cern.ch/user/s/snarayan/www/lxbatchmon/'+q+'.png')
